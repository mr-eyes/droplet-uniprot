export LC_ALL=C
echo "Extracting uniref90 headers ..."
zcat uniref90.fasta.gz | grep ">" > uniref90_headers
echo "DONE"
echo "Extracting mammalian headers ..."
cat uniref90_headers | grep -f mammalia_level2species.tsv | cut -c 2- > mammalian_headers
echo "DONE"
echo "Extracting mammalian_sequences ..."
zcat uniref90.fasta.gz | seqkit grep -n -f mammalian_headers > mammalian_sequences.fa